package logger

import "go.uber.org/zap"

var Log *zap.SugaredLogger

func init() {
	zapLogger, _ := zap.NewProduction()
	Log = zapLogger.Sugar()

}
func Close() {
	Log.Sync() // flushes buffer, if any
}
