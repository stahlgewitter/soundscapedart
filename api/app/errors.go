package app

import "errors"

var (
	ErrUnauthenticated = errors.New("Unauthenticated")
)
