package library

import (
	"context"
	"errors"
	"soundscapedArtApi/logger"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type LibraryDao interface {
	Create(ctx context.Context, lib Library) (string, error)
	Get(ctx context.Context, id string) (*Library, error)
	Update(ctx context.Context, c Library) error
}

type Library struct {
	Id          string `bson:"_id,omitempty" json:",omitempty"`
	Soundscapes []Soundscape
	Collections []Collection
}

type Soundscape struct {
	Id          string
	Name        string
	ExternalUrl string
	Url         string
}

type Collection struct {
	Name                    string
	ImageSoundscapeMappings []ImageSoundscapeMapping
}

type ImageSoundscapeMapping struct {
	ImageInfo    ImageInfo
	SoundscapeId string
}

type ImageInfo struct {
	ImageUrl       string
	ExternalId     string
	ExternalSource string
}

// libraryDao impl.
type libraryDaoMongo struct {
	libraryC *mongo.Collection
}

func NewLibraryDaoMongo(client *mongo.Client) LibraryDao {
	db := client.Database("soundscapedArt")
	libraryC := db.Collection("library")
	return &libraryDaoMongo{libraryC: libraryC}
}

func (c *libraryDaoMongo) Create(ctx context.Context, lib Library) (string, error) {
	return c.upsert(ctx, lib)
}

func (c *libraryDaoMongo) upsert(ctx context.Context, lib Library) (string, error) {
	objectId, err := primitive.ObjectIDFromHex(lib.Id)
	if err != nil {
		return "", err
	}

	// copy object here and clear id, otherwise we'd run into an error because the object's id type is string instead of the expected primitive.objectId
	// maybe better handle that through custom marshalling, but haven't found a good way, maybe custom Type, that implements marshalBSON
	shallowCopy := lib
	shallowCopy.Id = ""
	upsert := true
	result, err := c.libraryC.ReplaceOne(ctx, bson.M{"_id": objectId}, shallowCopy, &options.ReplaceOptions{Upsert: &upsert})
	if err != nil {
		return "", err
	}

	logger.Log.Infow("updated library",
		"id", lib.Id,
		"matchedCount", result.MatchedCount,
		"modifiedCount", result.ModifiedCount)

	return "", nil
}

func (c *libraryDaoMongo) Get(ctx context.Context, id string) (*Library, error) {
	if strings.TrimSpace(id) == "" {
		return nil, errors.New("userId must not be empty")
	}

	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	var lib Library
	if err = c.libraryC.FindOne(ctx, bson.D{{Key: "_id", Value: objectId}}).Decode(&lib); err != nil {
		return nil, err
	}
	return &lib, nil
}

func (c *libraryDaoMongo) Update(ctx context.Context, lib Library) error {
	_, err := c.upsert(ctx, lib)
	return err
}
