package library

import (
	"context"
	"errors"
	"soundscapedArtApi/user"
	"strings"
	"testing"
)

func TestCreateLibrarySucceedsForLoggedInUser(t *testing.T) {
	userId := "123"
	contextWithUserId := user.AddAuthDetailsToContext(context.Background(), user.AuthDetails{UserId: userId})

	i := newLibraryInteractorUsingInMemoryDaos()

	lib := Library{Soundscapes: []Soundscape{}, Collections: []Collection{}, Id: userId}
	_, err := i.Create(contextWithUserId, CreateLibraryRequest{Parent: userId, Library: lib})

	if err != nil {
		t.Fatalf("unexpected error creating library with logged in user, err=%v", err)
	}

	getLibResult, err := i.Get(contextWithUserId, GetLibraryRequest{Parent: userId})
	if err != nil {
		t.Fatalf("unexpected error getting created lib, err=%v", err)
	}

	if getLibResult.Id != lib.Id {
		t.Fatalf("lib ids differ, expected=%s, was=%s", lib.Id, getLibResult.Id)
	}
}

func TestCreateLibraryFailsIfNotLoggedIn(t *testing.T) {
	parentId := ""
	i := newLibraryInteractorUsingInMemoryDaos()
	lib := Library{Soundscapes: []Soundscape{}, Collections: []Collection{}, Id: parentId}
	_, err := i.Create(context.TODO(), CreateLibraryRequest{Parent: parentId, Library: lib})
	if err == nil {
		t.Fatalf("expected error for no logged in user specified in context")
	}
}

func TestCreateLibraryFailsIfRequestHasParentIdOtherThanLoggedInUser(t *testing.T) {
	userId := "123"
	contextWithUserId := user.AddAuthDetailsToContext(context.Background(), user.AuthDetails{UserId: userId})

	i := newLibraryInteractorUsingInMemoryDaos()

	lib := Library{Soundscapes: []Soundscape{}, Collections: []Collection{}, Id: userId}
	_, err := i.Create(contextWithUserId, CreateLibraryRequest{Parent: "differentParentId", Library: lib})

	if err == nil {
		t.Fatalf("expected error, but was none")
	}
}

func newLibraryInteractorUsingInMemoryDaos() LibraryInteractor {
	dao := newLibraryDaoInMemory()
	return NewMyLibraryInteractor(&dao)
}

// helpers

// LibraryDao impl
type libraryDaoInMemory struct {
	libraries map[string]Library
}

func newLibraryDaoInMemory() libraryDaoInMemory {
	return libraryDaoInMemory{libraries: map[string]Library{}}
}

func (dao *libraryDaoInMemory) Create(_ context.Context, lib Library) (string, error) {
	normalizedId := normalizeId(lib.Id)
	_, ok := dao.libraries[normalizedId]
	if ok {
		return "", errors.New("library already exists")
	}

	dao.libraries[normalizedId] = lib
	return "", nil
}

func (dao *libraryDaoInMemory) Get(_ context.Context, id string) (*Library, error) {
	normalizedId := normalizeId(id)
	lib, ok := dao.libraries[normalizedId]
	if !ok {
		return nil, nil
	}

	return &lib, nil
}

func (dao *libraryDaoInMemory) Update(_ context.Context, lib Library) error {
	normalizedId := normalizeId(lib.Id)
	_, ok := dao.libraries[normalizedId]
	if !ok {
		return errors.New("library not found")
	}

	dao.libraries[normalizedId] = lib
	return nil
}

func normalizeId(id string) string {
	return strings.ToUpper(strings.TrimSpace(id))
}
