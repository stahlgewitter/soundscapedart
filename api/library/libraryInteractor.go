package library

import (
	"context"
	"soundscapedArtApi/app"
	"soundscapedArtApi/user"
)

type LibraryInteractor interface {
	Create(ctx context.Context, rq CreateLibraryRequest) (string, error)
	Get(ctx context.Context, rq GetLibraryRequest) (*Library, error)
}

type CreateLibraryRequest struct {
	Parent  string // UserId
	Library Library
}

type GetLibraryRequest struct {
	Parent string // UserId
}

// impl
type MyLibraryInteractor struct {
	libraryDao LibraryDao
}

func NewMyLibraryInteractor(libraryDao LibraryDao) LibraryInteractor {
	return &MyLibraryInteractor{libraryDao: libraryDao}
}

func (i *MyLibraryInteractor) Create(ctx context.Context, rq CreateLibraryRequest) (string, error) {
	// only logged-in user may create library
	auth := user.GetAuthDetails(ctx)
	if auth == nil {
		return "", app.ErrUnauthenticated
	}

	// ensure only may create library collection for oneself
	if rq.Parent != auth.UserId {
		return "", app.ErrUnauthenticated
	}

	return i.libraryDao.Create(ctx, rq.Library)
}

func (i *MyLibraryInteractor) Get(ctx context.Context, rq GetLibraryRequest) (*Library, error) {
	return i.libraryDao.Get(ctx, rq.Parent)
}
