package user

import (
	"context"
	"crypto/rand"
	b64 "encoding/base64"
	"errors"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/argon2"
)

type AuthenticationDao interface {
	Authenticate(ctx context.Context, authDetails AuthenticationDetails) (bool, error)
	CreateAuthentication(ctx context.Context, authDetails AuthenticationDetails) error
}

type AuthenticationDetails interface {
	IsAuthenticationDetails()
}
type UsernamePasswordAuthenticationDetails struct {
	Username string
	Password string
}

type AuthEntry struct {
	Id           string `bson:"_id,omitempty"`
	Username     string `bson:"username"`
	PasswordHash string `bson:"passwordHash"` // base64
	PasswordSalt string `bson:"passwordSalt"` // base64
}

func (ad UsernamePasswordAuthenticationDetails) IsAuthenticationDetails() {}

// ### AuthenticationDao implementation

func (s *userDaoMongo) Authenticate(ctx context.Context, authDetails AuthenticationDetails) (bool, error) {
	auth := authDetails.(UsernamePasswordAuthenticationDetails) // currently, only supported type, so no type switching

	var authEntry AuthEntry
	err := s.authC.FindOne(ctx, bson.D{{Key: "username", Value: auth.Username}}).Decode(&authEntry)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return false, nil
		}
		return false, err
	}

	passwordSaltBytes, err := b64.StdEncoding.DecodeString(authEntry.PasswordSalt)
	if err != nil {
		return false, err
	}

	passwordHashFromArgInput := argon2.IDKey([]byte(auth.Password), passwordSaltBytes, 1, 64*1024, 4, PW_HASH_NUM_BYTES)
	passwordHashFromArgInputB64 := b64.StdEncoding.EncodeToString(passwordHashFromArgInput)
	return passwordHashFromArgInputB64 == authEntry.PasswordHash, nil
}

func (s *userDaoMongo) CreateAuthentication(ctx context.Context, authDetails AuthenticationDetails) error {
	auth := authDetails.(UsernamePasswordAuthenticationDetails) // currently, only supported type, so no type switching
	username := auth.Username
	password := auth.Password

	if username == "" || password == "" {
		return errors.New("username or password is empty")
	}

	// get user id for username (to use same id for auth collection so querying by is easy)
	// var userId primitive.ObjectID
	var result bson.M
	err := s.userC.FindOne(ctx, bson.D{{Key: "username", Value: username}}, &options.FindOneOptions{Projection: bson.M{"_id": 1}}).Decode(&result)
	if err != nil {
		// This error means your query did not match any documents.
		if err == mongo.ErrNoDocuments {
			return fmt.Errorf("username not found=%s", username)
		}
		return err
	}

	userId := result["_id"].(primitive.ObjectID)
	if userId == primitive.NilObjectID {
		return fmt.Errorf("unexpected NilObjectID for username search=%s", username)
	}

	// hash password
	passwordHash, err := hashPassword(password)
	if err != nil {
		return err
	}

	// store password hash
	_, err = s.authC.InsertOne(
		ctx,
		AuthEntry{
			Id:           userId.Hex(),
			Username:     username,
			PasswordHash: passwordHash.Hash,
			PasswordSalt: passwordHash.Salt})
	if err != nil {
		return err
	}

	return nil
}

// ### Util

type PasswordHash struct {
	Hash string // base64
	Salt string // base64
}

func hashPassword(password string) (*PasswordHash, error) {
	// argon2 recommended as password hashing method:
	// https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html
	// https://www.password-hashing.net
	passwordSalt := make([]byte, PW_SALT_NUM_BYTES)
	if _, err := rand.Read(passwordSalt); err != nil {
		return nil, err
	}
	// params taken from golang doc
	passwordHash := argon2.IDKey([]byte(password), passwordSalt, 1, 64*1024, 4, PW_HASH_NUM_BYTES)

	hashBase64 := b64.StdEncoding.EncodeToString(passwordHash)
	saltBase64 := b64.StdEncoding.EncodeToString(passwordSalt)

	return &PasswordHash{Hash: hashBase64, Salt: saltBase64}, nil
}
