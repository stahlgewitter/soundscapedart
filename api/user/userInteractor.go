package user

import (
	"context"
	"errors"
	"fmt"
	"soundscapedArtApi/logger"
	"strings"
)

type UserInteractor interface {
	CreateUser(ctx context.Context, rq CreateUserRequest) (string, error)
	GetUser(ctx context.Context, rq GetUserRequest) (*User, error)
	UpdateUser(ctx context.Context, rq UpdateUserRequest) error
	FindUser(ctx context.Context, rq FindUserRequest) (*User, error)
	Authenticate(ctx context.Context, rq AuthenticateUserRequest) (bool, error)
}

type CreateUserRequest struct {
	Username string
	Password string
}

type GetUserRequest struct {
	Id string
}

type AuthenticateUserRequest struct {
	Username string
	Password string
}

type FindUserRequest struct {
	Username string
}

type UpdateUserRequest struct {
	User User
}

type AlreadyExistsError struct {
	msg string
}

func (e *AlreadyExistsError) Error() string { return e.msg }

// impl
type MyUserInteractor struct {
	userDao       UserDao
	authenticator AuthenticationDao
}

func NewMyUserInteractor(userDao UserDao, authenticator AuthenticationDao) UserInteractor {
	return &MyUserInteractor{userDao: userDao, authenticator: authenticator}
}

func (i *MyUserInteractor) CreateUser(ctx context.Context, rq CreateUserRequest) (string, error) {
	logger.Log.Infow("CreateUser", "username", rq.Username)

	if strings.TrimSpace(rq.Username) == "" {
		return "", errors.New("username must not be empty or only whitespace")
	}

	if strings.TrimSpace(rq.Password) == "" {
		return "", errors.New("password must not be empty or only whitespace")
	}

	// TODO enforce password requirements (or in business logic stuff?)
	existingUser, err := i.userDao.FindUser(ctx, rq.Username)
	if err != nil {
		return "", err
	}
	if existingUser != nil {
		return "", &AlreadyExistsError{msg: fmt.Sprintf("user with name='%s' already exists", rq.Username)}
	}

	user := User{Username: rq.Username}
	userId, err := i.userDao.CreateUser(ctx, user)
	if err != nil {
		return "", err
	}

	err = i.authenticator.CreateAuthentication(ctx, UsernamePasswordAuthenticationDetails{Username: rq.Username, Password: rq.Password})
	if err != nil {
		// if it errors here, the user is created but the authentication store failed
		// TODO: handle that: maybe use tx to combine user creation and auth creation or delete written user
		return "", err
	}

	return userId, nil
}

func (i *MyUserInteractor) GetUser(ctx context.Context, rq GetUserRequest) (*User, error) {
	return i.userDao.GetUser(ctx, rq.Id)
}

func (i *MyUserInteractor) FindUser(ctx context.Context, rq FindUserRequest) (*User, error) {
	return i.userDao.FindUser(ctx, rq.Username)
}

func (i *MyUserInteractor) UpdateUser(ctx context.Context, rq UpdateUserRequest) error {
	// TODO: ensure we don't change user name
	return i.userDao.UpdateUser(ctx, rq.User)
}

func (i *MyUserInteractor) Authenticate(ctx context.Context, rq AuthenticateUserRequest) (bool, error) {
	r, err := i.authenticator.Authenticate(ctx, UsernamePasswordAuthenticationDetails{Username: rq.Username, Password: rq.Password})
	logger.Log.Infow("authenticate",
		"username", rq.Username,
		"success", r,
		"error", err)
	return r, err
}
