package user

import (
	"context"
	"net/http"

	"github.com/alexedwards/scs/v2"
)

type AuthDetails struct {
	UserId string
}

// context key type
type authDetailsCtxKeyType string

// context keys
var authDetailsCtxKey authDetailsCtxKeyType = "authDetails"

func GetAuthDetails(ctx context.Context) *AuthDetails {
	u, ok := ctx.Value(authDetailsCtxKey).(AuthDetails)
	if !ok {
		return nil
	}
	return &u
}

func AddAuthDetailsToContext(ctx context.Context, d AuthDetails) context.Context {
	return context.WithValue(ctx, authDetailsCtxKey, d)
}

func WriteAuthDetailsToCtxMiddlewareFac(sessionManager *scs.SessionManager) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			sessionUserId := sessionManager.Get(r.Context(), "UserId")
			if sessionUserId != nil {
				ctx := context.WithValue(r.Context(), authDetailsCtxKey, AuthDetails{UserId: sessionUserId.(string)})
				r = r.WithContext(ctx)
			}

			// call the next handler in the chain, passing the response writer and
			// the updated request object with the new context value.
			next.ServeHTTP(w, r)
		})
	}
}
