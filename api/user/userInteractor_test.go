package user

import (
	"context"
	"errors"
	"strings"
	"testing"
)

const validPassword = "validPassword123"

func TestCreateNewUserSucceeds(t *testing.T) {
	i := newUserInteractorUsingInMemoryDaos()
	c := context.TODO()
	username := "foo"

	user, _ := i.FindUser(c, FindUserRequest{Username: username})
	if user != nil {
		t.Fatalf("existing user not expected")
	}

	userId, err := i.CreateUser(c, CreateUserRequest{Username: username, Password: "bar"})
	if err != nil {
		t.Fatalf("unexpected err %v", err)
	}

	user, err = i.GetUser(c, GetUserRequest{Id: userId})
	if user == nil {
		t.Fatalf("created user with id=%s not found, err=%v", userId, err)
	}

	user, err = i.FindUser(c, FindUserRequest{Username: username})
	if user == nil {
		t.Fatalf("created user with id=%s not found, err=%v", userId, err)
	}
}

func TestCreateExistingUserFails(t *testing.T) {
	i := newUserInteractorUsingInMemoryDaos()
	c := context.TODO()
	username := "foo"

	// first creation should work
	_, err := i.CreateUser(c, CreateUserRequest{Username: username, Password: validPassword})
	if err != nil {
		t.Fatalf("unexpected err=%v", err)
	}

	// creation with identical username should fail
	t.Run("identical username", func(t *testing.T) {
		_, err = i.CreateUser(c, CreateUserRequest{Username: username, Password: validPassword})
		if err == nil {
			t.Fatalf("expected err when creating user with existing identical username")
		}
	})

	// different cased username should also fail
	t.Run("differently cased username", func(t *testing.T) {
		_, err = i.CreateUser(c, CreateUserRequest{Username: "fOo", Password: validPassword})
		if err == nil {
			t.Fatalf("expected err when creating user with existing username, just with different case")
		}
	})

	// untrimmed but otherwise same username should also fail
	t.Run("untrimmed username", func(t *testing.T) {
		_, err = i.CreateUser(c, CreateUserRequest{Username: " " + username + "  ", Password: validPassword})
		if err == nil {
			t.Fatalf("expected err when creating user with existing username, just with untrimmed username")
		}
	})
}

var invalidCreateUserRequests = []struct {
	createUserRequest CreateUserRequest
	desc              string
}{
	{
		createUserRequest: CreateUserRequest{Username: "", Password: validPassword},
		desc:              "empty username",
	},
	{
		createUserRequest: CreateUserRequest{Username: "   ", Password: validPassword},
		desc:              "whitespace username",
	},
	{
		createUserRequest: CreateUserRequest{Username: "validUsername", Password: ""},
		desc:              "empty password",
	},
	{
		createUserRequest: CreateUserRequest{Username: "validUsername", Password: "   "},
		desc:              "whitespace password",
	},
}

func TestCreateUserFailsWithBadRqData(t *testing.T) {
	i := newUserInteractorUsingInMemoryDaos()

	for _, testData := range invalidCreateUserRequests {
		t.Run(testData.desc, func(t *testing.T) {
			if _, err := i.CreateUser(context.TODO(), testData.createUserRequest); err == nil {
				t.Fatalf("expected err for user=%s, pass=%s, desc=%s", testData.createUserRequest.Username, testData.createUserRequest.Password, testData.desc)
			}
		})
	}
}

func TestAuthentication(t *testing.T) {
	i := newUserInteractorUsingInMemoryDaos()
	c := context.TODO()

	username := "foo"
	i.CreateUser(c, CreateUserRequest{Username: username, Password: validPassword})

	t.Run("correct credentials", func(t *testing.T) {
		ok, err := i.Authenticate(c, AuthenticateUserRequest{Username: username, Password: validPassword})
		if !ok || err != nil {
			t.Fatalf("authenticate failed, err=%v", err)
		}
	})

	t.Run("incorrect password", func(t *testing.T) {
		ok, err := i.Authenticate(c, AuthenticateUserRequest{Username: username, Password: "badPassword"})
		if ok {
			t.Fatalf("authenticate was unexpectedly successful")
		}
		if err != nil {
			t.Fatalf("authentication attempt with bad password resulted in unexpected error=%v", err)
		}
	})

	t.Run("no password", func(t *testing.T) {
		ok, err := i.Authenticate(c, AuthenticateUserRequest{Username: username, Password: ""})
		if ok {
			t.Fatalf("authenticate was unexpectedly successful")
		}
		if err != nil {
			t.Fatalf("authentication attempt with bad password resulted in unexpected error=%v", err)
		}
	})
}

// testing helpers: stubs

func newUserInteractorUsingInMemoryDaos() UserInteractor {
	userDao := newUserDaoInMemory()
	authDao := newAuthDaoInMemory()
	return NewMyUserInteractor(&userDao, &authDao)
}

// authDao impl
type authDaoInMemory struct {
	authDetails map[string]AuthenticationDetails
}

func newAuthDaoInMemory() authDaoInMemory {
	return authDaoInMemory{authDetails: map[string]AuthenticationDetails{}}
}

func (authDao *authDaoInMemory) Authenticate(ctx context.Context, authDetails AuthenticationDetails) (bool, error) {
	inputAuthData := authDetails.(UsernamePasswordAuthenticationDetails)

	v, ok := authDao.authDetails[inputAuthData.Username]
	if !ok {
		return false, nil
	}
	dbAuth := v.(UsernamePasswordAuthenticationDetails)

	if inputAuthData.Password == dbAuth.Password {
		return true, nil
	} else {
		return false, nil
	}
}

func (authDao *authDaoInMemory) CreateAuthentication(ctx context.Context, authDetails AuthenticationDetails) error {
	inputAuthData := authDetails.(UsernamePasswordAuthenticationDetails)
	authDao.authDetails[inputAuthData.Username] = inputAuthData
	return nil
}

// userDao impl
type userDaoInMemory struct {
	users map[string]User
}

func newUserDaoInMemory() userDaoInMemory {
	return userDaoInMemory{users: map[string]User{}}
}

func usernameToKey(username string) string {
	return strings.ToUpper(strings.TrimSpace(username))
}

func (userDao *userDaoInMemory) CreateUser(ctx context.Context, user User) (string, error) {
	key := usernameToKey(user.Username)
	if _, ok := userDao.users[key]; ok {
		return "", errors.New("user already exists")
	}

	userDao.users[key] = user
	return user.Id, nil
}

func (userDao *userDaoInMemory) GetUser(ctx context.Context, id string) (*User, error) {
	for _, user := range userDao.users {
		if user.Id == id {
			return &user, nil
		}
	}
	return nil, nil
}

func (userDao *userDaoInMemory) UpdateUser(ctx context.Context, user User) error {
	key := usernameToKey(user.Username)
	if _, ok := userDao.users[key]; !ok {
		return errors.New("user does not exists")
	}

	userDao.users[key] = user

	return nil
}

func (userDao *userDaoInMemory) FindUser(ctx context.Context, username string) (*User, error) {
	for _, user := range userDao.users {
		if usernameToKey(user.Username) == usernameToKey(username) {
			return &user, nil
		}
	}
	return nil, nil
}
