package user

import (
	"context"
	"errors"
	"fmt"
	"soundscapedArtApi/logger"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type UserDao interface {
	CreateUser(ctx context.Context, user User) (string, error)
	GetUser(ctx context.Context, id string) (*User, error)
	UpdateUser(ctx context.Context, user User) error
	FindUser(ctx context.Context, username string) (*User, error)
}

type User struct {
	Id       string `bson:"_id,omitempty" json:",omitempty"`
	Username string
}

const (
	// salt: 16 bytes is recommended for pw hashing:
	// see https://tools.ietf.org/html/draft-irtf-cfrg-argon2-04#section-3.1
	PW_SALT_NUM_BYTES = 16
	// 16 bytes or more is recommended
	PW_HASH_NUM_BYTES = 32
)

// ### UserDao impl
type userDaoMongo struct {
	client *mongo.Client
	userC  *mongo.Collection
	authC  *mongo.Collection
}

func NewUserDaoMongo(client *mongo.Client) UserDao {
	db := client.Database("soundscapedArt")
	userC := db.Collection("users")
	authC := db.Collection("auth")
	return &userDaoMongo{
		client: client,
		userC:  userC,
		authC:  authC}
}

func (s *userDaoMongo) CreateUser(ctx context.Context, user User) (string, error) {
	if strings.TrimSpace(user.Username) == "" {
		return "", errors.New("username must not be empty")
	}

	// store user
	r, err := s.userC.InsertOne(ctx, user)
	if err != nil {
		return "", err
	}

	insertedUserId := r.InsertedID.(primitive.ObjectID)

	return insertedUserId.Hex(), nil
}

func (s *userDaoMongo) GetUser(ctx context.Context, id string) (*User, error) {
	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	var user User
	if err := s.userC.FindOne(ctx, bson.D{{Key: "_id", Value: objectId}}).Decode(&user); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, nil
		}
		return nil, err
	}
	return &user, nil
}

func (s *userDaoMongo) FindUser(ctx context.Context, name string) (*User, error) {
	var user User
	// using find instead of findone, so we return nil instead of an error if the searched user doesn't exist
	f, err := s.userC.Find(ctx, bson.D{{Key: "username", Value: name}}, options.Find().SetCollation(caseInsensitiveCollation()))
	if err != nil {
		return nil, err
	}
	defer func(f *mongo.Cursor, ctx context.Context) {
		err := f.Close(ctx)
		if err != nil {
			logger.Log.Errorw("error closing mongo handler", "err", err)
		}
	}(f, ctx)

	if f.RemainingBatchLength() == 0 {
		return nil, nil
	}

	if !f.Next(ctx) {
		return nil, fmt.Errorf("unexpected ")
	}

	if err = f.Decode(&user); err != nil {
		return nil, err
	}

	return &user, nil
}

func (s *userDaoMongo) UpdateUser(ctx context.Context, user User) error {
	objectId, err := primitive.ObjectIDFromHex(user.Id)
	if err != nil {
		return err
	}

	// copy user here and clear id, otherwise we'd run into an error because the User id type is string instead of the expected primitive.objectId
	// maybe better handle that through custom marshalling, but haven't found a good way, maybe custom Type, that implements marshalBSON
	shallowUserCopy := user
	shallowUserCopy.Id = ""
	result, err := s.userC.ReplaceOne(ctx, bson.M{"_id": objectId}, shallowUserCopy, &options.ReplaceOptions{})
	if err != nil {
		return err
	}

	logger.Log.Infow("updated user",
		"id", user.Id,
		"username", user.Username,
		"matchedCount", result.MatchedCount,
		"modifiedCount", result.ModifiedCount)

	return nil
}

func caseInsensitiveCollation() *options.Collation {
	return &options.Collation{
		Locale:   "en",
		Strength: 1, // case insensitive if CaseLevel: false
	}
}
