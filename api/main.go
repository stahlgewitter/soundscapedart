package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"

	"net/http"
	"os"
	"soundscapedArtApi/app"
	"soundscapedArtApi/library"
	"soundscapedArtApi/logger"
	"soundscapedArtApi/user"
	"time"

	"github.com/alexedwards/scs/v2"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const apiVersion = "1.0"
const apiVersionUrlPart = "/v" + apiVersion

func connectMongoDb() (*mongo.Client, error) {
	uri := os.Getenv("MONGODB_URI")
	if uri == "" {
		uri = "mongodb://localhost/"
		// log.Fatal("You must set your 'MONGODB_URI' environmental variable. See\n\t https://docs.mongodb.com/drivers/go/current/usage-examples/#environment-variable")
	}
	return mongo.Connect(context.TODO(), options.Client().ApplyURI(uri))
}

func main() {
	// defer logging.Close()
	logger.Log.Info("Starting App")

	// init deps
	mongoClient, err := connectMongoDb()
	if err != nil {
		panic(err)
	}
	defer mongoClient.Disconnect(context.TODO())

	initMongoDb(mongoClient)

	// setup other deps
	userDao := user.NewUserDaoMongo(mongoClient)
	authenticator := userDao.(user.AuthenticationDao) // userDao also implements auth checking
	userInteractor := user.NewMyUserInteractor(userDao, authenticator)

	libraryDao := library.NewLibraryDaoMongo(mongoClient)
	libraryInteractor := library.NewMyLibraryInteractor(libraryDao)

	// Initialize a new session manager and configure the session lifetime.
	sessionManager := scs.New()
	sessionManager.Lifetime = 24 * time.Hour

	// setup routing and http server
	router := chi.NewRouter()

	// middleware
	router.Use(middleware.Heartbeat("/ping"))
	router.Use(middleware.Logger)
	router.Use(middleware.AllowContentType("application/json", "application/x-www-form-urlencoded"))
	router.Use(middleware.Compress(5, "application/json", "text/html", "text/css"))
	router.Use(middleware.Recoverer)
	router.Use(SetContentTypeHeaderToJson)
	router.Use(user.WriteAuthDetailsToCtxMiddlewareFac(sessionManager))

	// handlers
	router.Post(apiVersionUrlPart+"/users", CreateUserHandler(userInteractor))
	router.Get(apiVersionUrlPart+"/users/{userId}", GetUserHandler(userInteractor))
	router.Put(apiVersionUrlPart+"/users/{userId}", UpdateUserHandler(userInteractor))

	router.Post(apiVersionUrlPart+"/login", LoginHandler(userInteractor, sessionManager))
	router.Post(apiVersionUrlPart+"/logout", func(w http.ResponseWriter, r *http.Request) {
		err := sessionManager.Clear(r.Context())
		if err != nil {
			logger.Log.Errorw("error on logout", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})

	router.Get(apiVersionUrlPart+"/users/{userId}/library", GetLibraryHandler(libraryInteractor))
	router.Post(apiVersionUrlPart+"/users/{userId}/library", CreateLibraryHandler(libraryInteractor))
	router.Put(apiVersionUrlPart+"/users/{userId}/library", CreateLibraryHandler(libraryInteractor))

	// for testing
	router.Get(apiVersionUrlPart+"/whoami", func(w http.ResponseWriter, r *http.Request) {
		x := sessionManager.Get(r.Context(), "Username")
		var who string
		if x != nil {
			who = x.(string)
		} else {
			who = "?"
		}
		io.WriteString(w, who)
	})

	err = http.ListenAndServe(":3000", sessionManager.LoadAndSave(router))
	if err != nil {
		logger.Log.Errorw("", "err", err)
	}
}

func initMongoDb(mongoClient *mongo.Client) {
	// case insensitive, see https://docs.mongodb.com/manual/reference/collation/
	caseInsensitiveCollation := options.Collation{
		Locale:   "en",
		Strength: 1, // case insensitive if CaseLevel: false
	}

	usersCollectionName := "users"
	db := mongoClient.Database("soundscapedArt")
	userC := db.Collection(usersCollectionName)

	// build indexOptions
	indexOptions := options.Index()
	indexName := "usernameUnique"
	indexOptions.SetName(indexName)
	indexOptions.SetUnique(true)
	indexOptions.SetCollation(&caseInsensitiveCollation)

	index := mongo.IndexModel{Keys: bson.M{"username": 1}, Options: indexOptions}
	if _, err := userC.Indexes().CreateOne(context.TODO(), index); err != nil {
		logger.Log.Errorw("error on startup creating mongodb index", "indexName", index, "err", err)
	}
}

func CreateLibraryHandler(libraryInteractor library.LibraryInteractor) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// parse rq body
		var rq library.CreateLibraryRequest
		err := json.NewDecoder(r.Body).Decode(&rq)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		userIdFromUrlParam := chi.URLParam(r, "userId")
		if rq.Parent == "" {
			rq.Parent = userIdFromUrlParam
		}

		_, err = libraryInteractor.Create(r.Context(), rq)
		if err != nil {
			if errors.Cause(err) == app.ErrUnauthenticated {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}

			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func GetLibraryHandler(libraryInteractor library.LibraryInteractor) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userId := chi.URLParam(r, "userId")
		// TODO error handling
		lib, err := libraryInteractor.Get(r.Context(), library.GetLibraryRequest{Parent: userId})
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}

		err = json.NewEncoder(w).Encode(lib)
		if err != nil {
			logger.Log.Errorw("GetLibrary encoding response to json failed", "userId", userId, "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

func LoginHandler(userInteractor user.UserInteractor, sessionManager *scs.SessionManager) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// input
		var authUserRq user.AuthenticateUserRequest
		err := json.NewDecoder(r.Body).Decode(&authUserRq)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// try to auth
		success, err := userInteractor.Authenticate(r.Context(), authUserRq)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if !success {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		// fetch user to get its id (and ensure it exists)
		user, err := userInteractor.FindUser(r.Context(), user.FindUserRequest{Username: authUserRq.Username})
		if err != nil {
			logger.Log.Errorw("could not find user while login attempt",
				"username", authUserRq.Username,
				"err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if user == nil {
			logger.Log.Errorw("could authenticate but did not find user", "username", authUserRq.Username)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// set session cookie
		sessionManager.Put(r.Context(), "UserId", user.Id)
		sessionManager.Put(r.Context(), "Username", authUserRq.Username)
	}
}

func CreateUserHandler(userInteractor user.UserInteractor) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// input
		var createUserRq user.CreateUserRequest
		err := json.NewDecoder(r.Body).Decode(&createUserRq)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// process
		userId, err := userInteractor.CreateUser(r.Context(), createUserRq)
		if err != nil {
			_, ok := err.(*user.AlreadyExistsError)
			if ok {
				logger.Log.Infow("user already exists", "username", createUserRq.Username)
				w.WriteHeader(http.StatusConflict)
				return
			}
			logger.Log.Infow("error creating user", "username", createUserRq.Username, "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// output
		createdResourceLocation := apiVersionUrlPart + "/users/" + userId
		writeCreated(w, createdResourceLocation)
	}
}

func GetUserHandler(userInteractor user.UserInteractor) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// input
		var rq user.GetUserRequest = user.GetUserRequest{Id: chi.URLParam(r, "userId")}

		// process
		user, err := userInteractor.GetUser(r.Context(), rq)
		if err != nil {
			logger.Log.Errorw("getUser failed", "id", rq.Id, "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if user == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		// output
		err = json.NewEncoder(w).Encode(user)
		if err != nil {
			logger.Log.Errorw("GetUser encoding response to json failed", "id", rq.Id, "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

func UpdateUserHandler(userInteractor user.UserInteractor) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// input
		var userFromBody user.User
		err := json.NewDecoder(r.Body).Decode(&userFromBody)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// checking here OK because we only match userIdFromUrl from Url with id of user from body obj), not doing authentication or authorization
		userIdFromUrl := chi.URLParam(r, "userId")
		if userIdFromUrl != userFromBody.Id {
			http.Error(w, fmt.Sprintf("userId in path=%s and body=%s and doesn't match", userIdFromUrl, userFromBody.Id), http.StatusBadRequest)
			return
		}

		// update
		if err := userInteractor.UpdateUser(r.Context(), user.UpdateUserRequest{User: userFromBody}); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func writeCreated(w http.ResponseWriter, location string) {
	w.Header().Add("Location", location)
	w.WriteHeader(http.StatusCreated)
}

func SetContentTypeHeaderToJson(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		w.Header().Add("Content-Type", "application/json; charset=utf-8")

		// call the next handler in the chain, passing the response writer and
		// the updated request object with the new context value.
		//
		// note: context.Context values are nested, so any previously set
		// values will be accessible as well, and the new `"user"` key
		// will be accessible from this point forward.
		next.ServeHTTP(w, r)
	})
}
